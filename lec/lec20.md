
# Lecture 20: Overview, review and looking forward


## Exam in January

Monday 23 January, 9am, 2 hour exam

Computer Exam 1

*This information supersede what is stated in the module catalogue and previous in the notes*

### Computer Exam 1

You will be in a room on campus with a *Windows* university machine.

You will have access to:

- Minerva
- Gradescope
- Notes (comp2421-numerical-computation.gitlab.io/book)
- Jupyter
    - Binder **It is your responsibility to ensure the extenal service is available**
    - Apps anywhere  (see next slide)
- [Windows virtual desktop](http://wvd.leeds.ac.uk/) in case Apps anywhere doesn't load properly

You should bring:

- University ID card

Ahead of the exam you should read the [university guidence](https://students.leeds.ac.uk/info/10111/assessment/858/on_campus_examinations_preparation_and_arrangements).

### AppsAnywhere

[KB0014827: Getting started with AppsAnywhere](https://leeds.service-now.com/it?id=kb_article&sysparm_article=KB0014827)

**It is your responsibility to test you can open Jupyter Notebook via AppsAnywhere before the exam**

You can do this by:

- visiting a Windows cluster
- [KB0014379: Using Windows Virtual Desktop (WVD)](https://it.leeds.ac.uk/it?id=kb_article&sysparm_article=KB0014379)

You should ensure any files you require are already available before the exam

### Past papers

Last years past paper will be available soon.

Some exams from recent years are on minerva

-   These were set by someone else
-   They were closed book
-   They are too long
-   Ignore questions on Newton interpolation.

### Additional help

### Examinable material

In this module **everything** may be assessed:

-   All material in lectures;

-   All algorithmic details contained within the sample codes (that accompany the lectures, worksheets or coursework);

-   All material in formative worksheets and summative courseworks.

### What's expected and what to expect

I will expect you to be **able to apply** and **understand** all of the algorithms discussed:

-   I will expect you to understand when to use an algorithm.

-   I will expect you to understand how to analyse an algorithm either based on the algorithm itself or numerical results from the algorithm.

-   I will expect you to be able to understand variations and generalisations of algorithms, and to implement them.

-   I will expect you to understand and apply simple derivatives.

## Review

-   Floating point numbers
    -   Representations and rounding
    -   $eps$, machine precision

-   Matrices and vectors
    -   Multiplication
    -   Inner product
    -   Euclidean norms

-   Systems of linear equations
    -   Translating between matrices and systems of linear equations
    -   Deriving simple linear systems of equations
    -   Solving triangular systems of equations
    -   Gaussian Elimination
    -   LU factorisation
    -   Jacobi iteration
    -   Gauss Seidel iteration
    -   Initial guesses, convergence and stopping criteria

-   Dynamic problems
    -   Derivatives, gradients/slopes and rates of change
    -   Reading and drawing simple graphs
    -   Deriving models for simple dynamic problems
    -   Initial conditions
    -   Euler's method
    -   Midpoint scheme

-   Nonlinear equations
    -   Roots of an equation
    -   Bisection algorithm
    -   Newton's method
    -   Quasi Newton methods
    -   Hybrid methods

-   Data fitting
    -   How to form a system of linear equations to find a simple curve of best fit
    -   How to find a best fit solution
    -   When to choose which curve (from a simple choice)
    -   Problems with our approach

### Python

For this module, you are expected to be able to read, understand and construct simple scripts and functions, to implement simple algorithms, or to modify algorithms for your needs, and to be able to present and interpret the outputs from these. To this end, you should be familiar with elementary python notation and syntax (in particular, vectors, matrices, indices, loops and branches).

**You will not need to write any code during the exam. All questions can be answered using pen and paper.**

**You may prefer to use your own/the provided solutions to help answer some questions. It is your responsibility to make sure the code does what it should.**

## What next....?

-   You will apply ideas from this course in many areas of computer science including

    -   graphics;
    -   artificial intelligence/machine learning.

-   There are plenty of opportunities for Final Year Projects on topics in numerical computation. If you are interested talk to me (or your tutorial leader).

-   There are also funded places for further study in numerical computation....

### 

**Thank you for your attention!**

Good luck with your exam and future studies

I hope you have a good Christmas break!

The [slides used in the lecture](./lec20_.ipynb) are also available
